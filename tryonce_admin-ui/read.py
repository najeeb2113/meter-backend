import pandas as pd
from orm import session, Alert,StationFlow,DmaUfw
def flowprocess ():
    data=pd.read_csv("tempfiles/flow.csv")
    flow =[]
    station=[]
    date=[]
    mtype=[]
    data_dict = data.to_dict()

    flow = data_dict['flow']
    mtype = data_dict['measurementType']
    station = data_dict['line_id']
    date = data_dict['date']
    z=0
    for a in flow:
        # print(flow[z],dma[z],area[z],station[z],date[z])
        dbadd = StationFlow(line_id=station[z], date=date[z],measurementType=mtype[z], flow=flow[z])
        session.add(dbadd)
        session.commit()
        z=z+1
    return

def alertprocess ():
    data=pd.read_csv("tempfiles/alert.csv")
    # id =[]
    date = []
    station = []
    dma =[]
    alertreason = []
    remarks=[]
    alerttype=[]
    data_dict = data.to_dict()
    # id = data_dict['id']
    date = data_dict['date']
    station = data_dict['station_id']
    dma = data_dict['dma_id']
    alertreason = data_dict['alert_reason']
    remarks = data_dict['remarks']
    alerttype = data_dict['alert_type']
    z=0
    for a in station:
        # print(dma[z],station[z],date[z],alertreason[z],remarks[z],alerttype[z])
        dbadd = Alert(date=date[z], station_id=station[z], dma_id=dma[z], alert_reason=alertreason[z],remarks=remarks[z],alert_type=alerttype[z])
        session.add(dbadd)
        session.commit()
        z=z+1
    return


def dmaprocess ():
    data=pd.read_csv("tempfiles/dma.csv")
    # id =[]
    dma = []
    yearmonth =[]
    metersread = []
    readperiod=[]
    avgreaddays=[]
    inflow=[]
    consumption=[]
    ufw=[]
    ufwper=[]
    data_dict = data.to_dict()
    # id = data_dict['id']
    dma = data_dict['dma_id']
    yearmonth = data_dict['year_month']
    metersread = data_dict['read_meters']
    readperiod = data_dict['read_period']
    avgreaddays = data_dict['avg_read_days']
    inflow = data_dict['inflow']
    consumption = data_dict['consumption']
    ufw = data_dict['ufw']
    ufwper = data_dict['ufwper']
    z=0
    for a in ufw:
        # print(dma[z],yearmonth[z],metersread[z],readperiod[z],avgreaddays[z],inflow[z],consumption[z],ufw[z],ufwper[z])
        dbadd = DmaUfw(dma_id =dma[z],year_month=yearmonth[z],read_meters=metersread[z],read_period=readperiod[z],avg_read_days=avgreaddays[z],inflow=inflow[z],consumption=consumption[z],ufw=ufw[z],ufwper=ufwper[z])
        session.add(dbadd)
        session.commit()
        z=z+1
    return
