from sqlalchemy import Column, ForeignKey, Integer, String, BigInteger, TIMESTAMP, text, Boolean, TEXT, TIME, DATE, Float
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, scoped_session
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
import configparser
from flask_login import UserMixin


config = configparser.ConfigParser()
config.read('config.ini')

dbhost = config['DATABASE']['IP']
dbuser = config['DATABASE']['USERNAME']
dbpass = config['DATABASE']['PASSWORD']
dbname = config['DATABASE']['DB']
dbport = int(config['DATABASE']['PORT'])
Base = declarative_base()


class Subscriber(Base,UserMixin):
    __tablename__='admin'
    id = Column(BigInteger, primary_key=True)
    fullname = Column(String(50), nullable=False)
    email = Column(String(50), nullable=False, unique=True)
    password = Column(String(150), nullable=False)
    mobile = Column(String(20), nullable=False, unique=True)
    isadmin = Column(String(20), nullable=False)

    def __unicode__(self):
        return str(self.id)

    def __repr__(self):
        return str(self.id)


class Alert(Base):
    __tablename__ = 'alert'
    id = Column(BigInteger, primary_key=True)
    date = Column(DATE, nullable=True)
    station_id = Column(String(50),nullable=True)
    dma_id = Column(String(50),nullable=True)
    alert_reason = Column(String(100), nullable=True)
    remarks = Column(String(450), nullable=True)
    alert_type = Column(String(20), nullable=True)
    def __unicode__(self):
        return str(self.id)

    def __repr__(self):
        return str(self.id)


class DmaUfw(Base):
    __tablename__ = 'dmaufw'
    id = Column(BigInteger, primary_key=True)
    dma_id = Column(String(50),nullable=True)
    year_month = Column(String(30), nullable=True)
    read_meters = Column(BigInteger, nullable=True)
    read_period = Column(String(30), nullable=True)
    avg_read_days = Column(BigInteger, nullable=True)
    inflow = Column(BigInteger, nullable=True)
    consumption = Column(BigInteger, nullable=True)
    ufw = Column(BigInteger, nullable=True)
    ufwper = Column(Float, nullable=True)

    def __unicode__(self):
        return str(self.id)

    def __repr__(self):
        return str(self.id)


class StationFlow(Base):
    __tablename__ = 'stationflow'
    id = Column(BigInteger, primary_key=True)
    line_id = Column(String(30), nullable=True)
    date = Column(String(40), nullable=True)
    measurementType = Column(String(30), nullable=True)
    flow = Column(Integer, nullable=True)

    def __unicode__(self):
        return str(self.id)

    def __repr__(self):
        return str(self.id)    
    

class DMAList(Base):
    __tablename__ = 'dmalist'
    id = Column(BigInteger, primary_key=True)
    dma_id = Column(String(50),nullable=False)
    areaname = Column(String(50),nullable=True)
    boundary1 = Column(String(50),nullable=True)
    boundary2 = Column(String(50),nullable=True)
    boundary3 = Column(String(50),nullable=True)
    boundary4 = Column(String(50),nullable=True)
    areacolor = Column(String(10),nullable=True)


class StationList(Base):
    __tablename__ = 'stationlist'
    id = Column(BigInteger, primary_key=True)
    station_id = Column(String(50),nullable=False)
    dma_id = Column(String(50),nullable=False)
    station_name = Column(String(50),nullable=True)
    lineidentifier = Column(String(20),nullable=True)
    station_type = Column(String(20),nullable=True)
    latitude = Column(String(50), nullable=True)
    longitude = Column(String(50), nullable=True)

#Line Identifier and Station_type will be a drop down of a static list items. For now put Line Identifier as Line 1, Line 2, Line 3, Line 4. Station Type as Type 1, Type 2, Type 3


engine = create_engine('mysql+pymysql://{}:{}@{}:{}/{}'.format(config['DATABASE']['USERNAME'],config['DATABASE']['PASSWORD'],config['DATABASE']['IP'],config['DATABASE']['PORT'],config['DATABASE']['DB']),pool_recycle=280,isolation_level="READ UNCOMMITTED",pool_size=20, max_overflow=0)
Session = sessionmaker(bind=engine)
session = scoped_session(Session)
Base.metadata.create_all(engine)
