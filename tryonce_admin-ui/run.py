
import boto3
import flask_login
from boto3.dynamodb.conditions import Key
from botocore.exceptions import ClientError
from flask import Flask, request, redirect, render_template, url_for, flash
from waitress import serve
import random
import string
from flask_login import (
    login_required,
    login_user,
    logout_user,
    current_user,
    LoginManager
)
import os
import logging
from datetime import datetime

from passlib.hash import pbkdf2_sha256
import configparser
from boto3 import resource
from boto3.dynamodb.conditions import Key

app = Flask(__name__)

UPLOAD_FOLDER = 'tempfiles/'

app.config['SECRET_KEY'] = 'secret key'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
app.jinja_env.add_extension('jinja2.ext.loopcontrols')

login_manager = LoginManager()
login_manager.init_app(app)


config = configparser.ConfigParser()
config.read('config.ini')

api_key = config['API']['KEY']

admin_username = config['ADMIN']['username']
admin_password = config['ADMIN']['password']

bucket = config['AWS']['bucket']
region_name = config['AWS']['region_name']
aws_access_key_id = config['AWS']['aws_access_key_id']
aws_secret_access_key = config['AWS']['aws_secret_access_key']
objectpath = config['AWS']['objectpath']

dynamodb_resource = resource('dynamodb', region_name=region_name, aws_access_key_id=aws_access_key_id,
                          aws_secret_access_key=aws_secret_access_key)
cityadmin_id = 16
# new code is from ************* here *************************************************************


@app.route('/homeview', methods=['GET', 'POST'])
def homeview():
    client = boto3.client('dynamodb', region_name=region_name, aws_access_key_id=aws_access_key_id,
                          aws_secret_access_key=aws_secret_access_key)
    response = client.scan(
        TableName='MeterReadings-uh6sjlqw7ber3bo5oprxgc6px4-dev'
    )
    print(response)
    return render_template("homeview.html", active='active',data=response["Items"])



@app.route('/viewlocation/<uid>', methods=['GET', 'POST'])
def viewlocation(uid):
    client = boto3.client('dynamodb', region_name=region_name, aws_access_key_id=aws_access_key_id,
                          aws_secret_access_key=aws_secret_access_key)
    #response = client.scan(
     #   TableName='MeterConsumerByLocations-uh6sjlqw7ber3bo5oprxgc6px4-dev'
    #)
    response = client.query(
    # Add the name of the index you want to use in your query.
        TableName='MeterConsumerByLocations-uh6sjlqw7ber3bo5oprxgc6px4-dev',
        IndexName="MeterConsumerByUserId",
        ExpressionAttributeValues={
       ':v1': {
           'S': str(uid),
       },
   },
   KeyConditionExpression='user_id = :v1', ScanIndexForward=False,
    )
    print(response)
    return render_template("locationview.html", active='active',data=response["Items"])










@app.route('/homedealsdelete/<cid>', methods=['GET', 'POST'])
def homedealsdelete(cid):
    client = boto3.client('dynamodb', region_name=region_name, aws_access_key_id=aws_access_key_id,
                          aws_secret_access_key=aws_secret_access_key)

    client.delete_item(
        TableName='TryonceTopDeals-tloujol3mvdsveoacsa5zlf5fa-tryprod',
        Key={'id': {"S": cid}}
    )
    return redirect(url_for("homeview"))


@app.route('/homedailydelete/<cid>', methods=['GET', 'POST'])
def homedailydelete(cid):
    client = boto3.client('dynamodb', region_name=region_name, aws_access_key_id=aws_access_key_id,
                          aws_secret_access_key=aws_secret_access_key)

    client.delete_item(
        TableName='TryonceDailyEssentials-tloujol3mvdsveoacsa5zlf5fa-tryprod',
        Key={'id': {"S": cid}}
    )
    return redirect(url_for("homeview"))


@app.route('/homesubdelete/<cid>', methods=['GET', 'POST'])
def homesubdelete(cid):
    client = boto3.client('dynamodb', region_name=region_name, aws_access_key_id=aws_access_key_id,
                          aws_secret_access_key=aws_secret_access_key)

    client.delete_item(
        TableName='TryonceSubscriptionsoftheday-tloujol3mvdsveoacsa5zlf5fa-tryprod',
        Key={'id': {"S": cid}}
    )
    return redirect(url_for("homeview"))


@app.route('/ordersview', methods=['GET', 'POST'])
def ordersview():
    client = boto3.client('dynamodb', region_name=region_name, aws_access_key_id=aws_access_key_id,
                          aws_secret_access_key=aws_secret_access_key)
    response = client.scan(
        TableName='TryonceSubscriptions-tloujol3mvdsveoacsa5zlf5fa-tryprod'
    )
    response2 = client.scan(
        TableName='TryonceProducts-tloujol3mvdsveoacsa5zlf5fa-tryprod'
    )
    response3 = client.scan(
        TableName='TryonceUserAddresses-tloujol3mvdsveoacsa5zlf5fa-tryprod'
    )

    return render_template("ordersview.html", active4='active', data3=response3["Items"], data=response["Items"],
                           data2=response2["Items"])


@app.route('/ordersdelete/<cid>', methods=['GET', 'POST'])
def ordersdelete(cid):
    client = boto3.client('dynamodb', region_name=region_name, aws_access_key_id=aws_access_key_id,
                          aws_secret_access_key=aws_secret_access_key)

    client.delete_item(
        TableName='TryonceSubscriptions-tloujol3mvdsveoacsa5zlf5fa-tryprod',
        Key={'id': {"S": cid}}
    )
    return redirect(url_for("ordersview"))


@app.route('/subscriptionsview', methods=['GET', 'POST'])
def subscriptionsview():
    client = boto3.client('dynamodb', region_name=region_name, aws_access_key_id=aws_access_key_id,
                          aws_secret_access_key=aws_secret_access_key)
    response = client.scan(
        TableName='TryonceSubscriptions-tloujol3mvdsveoacsa5zlf5fa-tryprod'
    )
    response2 = client.scan(
        TableName='TryonceProducts-tloujol3mvdsveoacsa5zlf5fa-tryprod'
    )
    response3 = client.scan(
        TableName='TryonceUserAddresses-tloujol3mvdsveoacsa5zlf5fa-tryprod'
    )

    return render_template("subscriptionsview.html", active5='active', data3=response3["Items"], data=response["Items"],
                           data2=response2["Items"])


@app.route('/subscriptionsdelete/<cid>', methods=['GET', 'POST'])
def subscriptionsdelete(cid):
    client = boto3.client('dynamodb', region_name=region_name, aws_access_key_id=aws_access_key_id,
                          aws_secret_access_key=aws_secret_access_key)

    client.delete_item(
        TableName='TryonceSubscriptions-tloujol3mvdsveoacsa5zlf5fa-tryprod',
        Key={'id': {"S": cid}}
    )
    return redirect(url_for("subscriptionsview"))


@app.route('/bannersview', methods=['GET', 'POST'])
def bannersview():
    client = boto3.client('dynamodb', region_name=region_name, aws_access_key_id=aws_access_key_id,
                          aws_secret_access_key=aws_secret_access_key)
    catbanner = client.scan(
        TableName='TryonceBanners-tloujol3mvdsveoacsa5zlf5fa-tryprod'
    )
    homebanner = client.scan(
        TableName='TryonceHomebanner-tloujol3mvdsveoacsa5zlf5fa-tryprod'
    )
    homebanner2 = client.scan(
        TableName='TryonceHome2banner-tloujol3mvdsveoacsa5zlf5fa-tryprod'
    )
    category = client.scan(
        TableName='TryonceCategories-tloujol3mvdsveoacsa5zlf5fa-tryprod'
    )

    return render_template("bannersview.html", active6='active', catbanner=catbanner["Items"],
                           homebanner=homebanner["Items"], homebanner2=homebanner2["Items"], category=category["Items"])


@app.route('/catbannerdelete/<cid>', methods=['GET', 'POST'])
def catbannerdelete(cid):
    client = boto3.client('dynamodb', region_name=region_name, aws_access_key_id=aws_access_key_id,
                          aws_secret_access_key=aws_secret_access_key)

    client.delete_item(
        TableName='TryonceBanners-tloujol3mvdsveoacsa5zlf5fa-tryprod',
        Key={'id': {"S": cid}}
    )
    return redirect(url_for("bannersview"))


@app.route('/homebannerdelete/<cid>', methods=['GET', 'POST'])
def homebannerdelete(cid):
    client = boto3.client('dynamodb', region_name=region_name, aws_access_key_id=aws_access_key_id,
                          aws_secret_access_key=aws_secret_access_key)

    client.delete_item(
        TableName='TryonceHomebanner-tloujol3mvdsveoacsa5zlf5fa-tryprod',
        Key={'id': {"S": cid}}
    )
    return redirect(url_for("bannersview"))


@app.route('/homebanner2delete/<cid>', methods=['GET', 'POST'])
def homebanner2delete(cid):
    client = boto3.client('dynamodb', region_name=region_name, aws_access_key_id=aws_access_key_id,
                          aws_secret_access_key=aws_secret_access_key)

    client.delete_item(
        TableName='TryonceHome2banner-tloujol3mvdsveoacsa5zlf5fa-tryprod',
        Key={'id': {"S": cid}}
    )
    return redirect(url_for("bannersview"))


@app.route('/addbanner', methods=['GET', 'POST'])
def addbanner():
    if request.method == "GET":
        client = boto3.client('dynamodb', region_name=region_name, aws_access_key_id=aws_access_key_id,
                              aws_secret_access_key=aws_secret_access_key)
        response = client.scan(
            TableName='TryonceCategories-tloujol3mvdsveoacsa5zlf5fa-tryprod'
        )

        return render_template('addbanner.html', active6='active', data=response["Items"])

    if request.method == "POST":
        banner_id = ''.join(random.choices(string.digits, k=9))
        banner = request.form['banner']
        banner_image = request.files['banner_image']
        banner_name = request.form['banner_name']
        keyword = request.form['keyword']
        catfilename = request.files['banner_image'].filename
        catfilename = banner_id + catfilename
        category_id = ""
        status = upload_file(banner_image, catfilename, bucket)

        if banner == "3":
            category_id = request.form['category_id']
            if status:
                client = boto3.client('dynamodb', region_name=region_name, aws_access_key_id=aws_access_key_id,
                                      aws_secret_access_key=aws_secret_access_key)
                response = client.put_item(
                    TableName='TryonceBanners-tloujol3mvdsveoacsa5zlf5fa-tryprod',
                    Item={
                        'id': {"S": banner_id},
                        'banner_id': {"S": banner_id},
                        'cityadmin_id': {"S": str(cityadmin_id)},
                        'bannerloc_id': {"S": str(2)},
                        'category_id': {"S": category_id},
                        'banner_name': {"S": banner_name},
                        'banner_image': {"S": catfilename},
                        'keyword':  {"S": keyword},
                        '_version': {"S": str(1)},
                        'createdAt': {"S": datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%fZ")},
                        'updatedAt': {"S": datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%fZ")}
                    })

                return redirect(url_for("bannersview"))
            else:
                return redirect(url_for("addbanner"))

        elif banner == "2":
            if status:
                client = boto3.client('dynamodb', region_name=region_name, aws_access_key_id=aws_access_key_id,
                                      aws_secret_access_key=aws_secret_access_key)
                response = client.put_item(
                    TableName='TryonceHome2banner-tloujol3mvdsveoacsa5zlf5fa-tryprod',
                    Item={
                        'id': {"S": banner_id},
                        'banner_id': {"S": banner_id},
                        'cityadmin_id': {"S": str(cityadmin_id)},
                        'bannerloc_id': {"S": str(2)},
                        'banner_name': {"S": banner_name},
                        'banner_image': {"S": catfilename},
                        'keyword': {"S": keyword},
                        '_version': {"S": str(1)},
                        'createdAt': {"S": datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%fZ")},
                        'updatedAt': {"S": datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%fZ")}
                    })
                print(response)
                return redirect(url_for("bannersview"))
            else:
                return redirect(url_for("addbanner"))

        elif banner == "1":
            if status:
                client = boto3.client('dynamodb', region_name=region_name, aws_access_key_id=aws_access_key_id,
                                      aws_secret_access_key=aws_secret_access_key)
                response = client.put_item(
                    TableName='TryonceHomebanner-tloujol3mvdsveoacsa5zlf5fa-tryprod',
                    Item={
                        'id': {"S": banner_id},
                        'banner_id': {"S": banner_id},
                        'cityadmin_id': {"S": str(cityadmin_id)},
                        'bannerloc_id': {"S": str(2)},
                        'banner_name': {"S": banner_name},
                        'banner_image': {"S": catfilename},
                        'keyword': {"S": keyword},
                        '_version': {"S": str(1)},
                        'createdAt': {"S": datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%fZ")},
                        'updatedAt': {"S": datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%fZ")}
                    })

                return redirect(url_for("bannersview"))
            else:
                return redirect(url_for("addbanner"))


@app.route('/deliveryview', methods=['GET', 'POST'])
def deliveryview():
    client = boto3.client('dynamodb', region_name=region_name, aws_access_key_id=aws_access_key_id,
                          aws_secret_access_key=aws_secret_access_key)
    response = client.scan(
        Limit=2000,
        IndexName = 'delivery_date-index',
        TableName='TryonceSubscriptions-tloujol3mvdsveoacsa5zlf5fa-tryprod'
    )
    response2 = client.scan(
        TableName='TryonceProducts-tloujol3mvdsveoacsa5zlf5fa-tryprod'
    )
    response3 = client.scan(
        TableName='TryonceUserAddresses-tloujol3mvdsveoacsa5zlf5fa-tryprod'
    )
    city = client.scan(
        TableName='TryonceCities-tloujol3mvdsveoacsa5zlf5fa-tryprod'
    )
    area = client.scan(
        TableName='TryonceAreas-tloujol3mvdsveoacsa5zlf5fa-tryprod'
    )
    user = client.scan(
        TableName='TryonceUsers-tloujol3mvdsveoacsa5zlf5fa-tryprod'
    )
    deliveryboy = client.scan(
        TableName='TryonceDeliveryBoy-tloujol3mvdsveoacsa5zlf5fa-tryprod'
    )

    return render_template("deliveryview.html", active8='active', data3=response3["Items"], data=response["Items"],
                           data2=response2["Items"],city=city["Items"],area=area["Items"],users=user["Items"],deliveryboy=deliveryboy["Items"])


@app.route('/completed', methods=['GET', 'POST'])
def completed_orders():
    client = boto3.client('dynamodb', region_name=region_name, aws_access_key_id=aws_access_key_id,
                          aws_secret_access_key=aws_secret_access_key)
    response = client.scan(
        Limit=2000,
        IndexName = 'delivery_date-index',
        TableName='TryonceSubscriptions-tloujol3mvdsveoacsa5zlf5fa-tryprod'
    )
    response2 = client.scan(
        TableName='TryonceProducts-tloujol3mvdsveoacsa5zlf5fa-tryprod'
    )
    response3 = client.scan(
        TableName='TryonceUserAddresses-tloujol3mvdsveoacsa5zlf5fa-tryprod'
    )
    city = client.scan(
        TableName='TryonceCities-tloujol3mvdsveoacsa5zlf5fa-tryprod'
    )
    area = client.scan(
        TableName='TryonceAreas-tloujol3mvdsveoacsa5zlf5fa-tryprod'
    )
    user = client.scan(
        TableName='TryonceUsers-tloujol3mvdsveoacsa5zlf5fa-tryprod'
    )
    deliveryboy = client.scan(
        TableName='TryonceDeliveryBoy-tloujol3mvdsveoacsa5zlf5fa-tryprod'
    )

    return render_template("completed_orders.html", active8='active', data3=response3["Items"], data=response["Items"],
                           data2=response2["Items"],city=city["Items"],area=area["Items"],users=user["Items"],deliveryboy=deliveryboy["Items"])


@app.route('/assign', methods=['GET', 'POST'])
def assign():
    print(request.method)
    subs_id = request.form.get('subs_id')
    delivery_boy_id = request.form.get('delivery_boy_id')
    print(delivery_boy_id,subs_id)

    client = boto3.client('dynamodb', region_name=region_name, aws_access_key_id=aws_access_key_id,
                          aws_secret_access_key=aws_secret_access_key)
    client.update_item(
        TableName='TryonceSubscriptions-tloujol3mvdsveoacsa5zlf5fa-tryprod',
        Key={'id': {"S": subs_id}},
        UpdateExpression="set delivery_boy_id=:v, delivery_boy_incentive=:a, updatedAt=:u",
        ExpressionAttributeValues={
            ':v': {"S": delivery_boy_id},
            ':a': {"S": "1"},
            ':u': {"S": datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%fZ")}
        },
        ReturnValues="UPDATED_NEW",
    )

    return redirect(url_for('deliveryview'))

@app.route('/paymentsview', methods=['GET', 'POST'])
def paymentsview():
    client = boto3.client('dynamodb', region_name=region_name, aws_access_key_id=aws_access_key_id,
                          aws_secret_access_key=aws_secret_access_key)
    data = client.scan(
        TableName='TryonceRechargeHistory-tloujol3mvdsveoacsa5zlf5fa-tryprod'
    )
    data2 = client.scan(
        TableName='TryonceUsers-tloujol3mvdsveoacsa5zlf5fa-tryprod'
    )

    return render_template("paymentsview.html", active9='active', data=data["Items"], data2=data2["Items"])


@app.route('/paymentsdelete/<cid>', methods=['GET', 'POST'])
def paymentsdelete(cid):
    client = boto3.client('dynamodb', region_name=region_name, aws_access_key_id=aws_access_key_id,
                          aws_secret_access_key=aws_secret_access_key)

    client.delete_item(
        TableName='TryonceRechargeHistory-tloujol3mvdsveoacsa5zlf5fa-tryprod',
        Key={'id': {"S": cid}}
    )
    return redirect(url_for("paymentsview"))


@app.route('/deliveryboyview', methods=['GET', 'POST'])
def deliveryboyview():
    client = boto3.client('dynamodb', region_name=region_name, aws_access_key_id=aws_access_key_id,
                          aws_secret_access_key=aws_secret_access_key)
    data = client.scan(
        TableName='TryonceDeliveryBoy-tloujol3mvdsveoacsa5zlf5fa-tryprod'
    )
    return render_template("deliveryboyview.html", active11='active', data=data["Items"])


@app.route('/adddeliveryboy', methods=['GET', 'POST'])
def adddeliveryboy():
    if request.method == "GET":
        client = boto3.client('dynamodb', region_name=region_name, aws_access_key_id=aws_access_key_id,
                              aws_secret_access_key=aws_secret_access_key)
        area = client.scan(
            TableName='TryonceAreas-tloujol3mvdsveoacsa5zlf5fa-tryprod'
        )

        return render_template('adddeliveryboy.html', active11='active', area=area["Items"])
    if request.method == "POST":
        id = ''.join(random.choices(string.digits, k=9))
        area_id = request.form['area_id']
        delivery_boy_name = request.form['delivery_boy_name']
        delivery_boy_image = request.files['delivery_boy_image']
        delivery_boy_phone = request.form['delivery_boy_phone']
        delivery_boy_pass = request.form['delivery_boy_pass']

        catfilename = request.files['delivery_boy_image'].filename
        catfilename = id + catfilename

        status = upload_file(delivery_boy_image, catfilename, bucket)
        # status will be true if success
        if status:
            client = boto3.client('dynamodb', region_name=region_name, aws_access_key_id=aws_access_key_id,
                                  aws_secret_access_key=aws_secret_access_key)
            response = client.put_item(
                TableName='TryonceDeliveryBoy-tloujol3mvdsveoacsa5zlf5fa-tryprod',
                Item={
                    'id': {"S": id},
                    'delivery_boy_id': {"S": id},
                    'area_id': {"S": area_id},
                    'delivery_boy_name': {"S": delivery_boy_name},
                    'delivery_boy_image': {"S": catfilename},
                    'delivery_boy_phone': {"S": delivery_boy_phone},
                    'cityadmin_id': {"S": str(cityadmin_id)},
                    'delivery_boy_pass': {"S": delivery_boy_pass},
                    'lat': {"S": str(0.0000)},
                    'lng': {"S": str(0.0000)},
                    'device_id': {"S": str(''.join(random.choices(string.digits, k=11)))},
                    'delivery_boy_status': {"S": str("online")},
                    'is_confirmed': {"S": str(1)},
                    'otp': {"S": str("NULL")},
                    'phone_verify': {"S": str(1)},
                    '_version': {"S": str(1)},
                    'createdAt': {"S": datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%fZ")},
                    'updatedAt': {"S": datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%fZ")}
                })

            return redirect(url_for("deliveryboyview"))
        else:
            return redirect(url_for("adddeliveryboy"))


@app.route('/deliveryboyedit/<cid>', methods=['GET', 'POST'])
def deliveryboyedit(cid):
    client = boto3.client('dynamodb', region_name=region_name, aws_access_key_id=aws_access_key_id,
                          aws_secret_access_key=aws_secret_access_key)

    deliveryboy = client.get_item(
        TableName='TryonceDeliveryBoy-tloujol3mvdsveoacsa5zlf5fa-tryprod',
        Key={'id': {"S": cid}}
    )

    area = client.scan(
        TableName='TryonceAreas-tloujol3mvdsveoacsa5zlf5fa-tryprod'
    )

    return render_template("editdeliveryboy.html", active11='active', area=area["Items"], dataa=deliveryboy["Item"])


@app.route('/editdeliveryboy', methods=['GET', 'POST'])
def editdeliveryboy():
    id = request.form['id']
    area_id = request.form['area_id']
    delivery_boy_name = request.form['delivery_boy_name']
    delivery_boy_image = request.files['delivery_boy_image']
    delivery_boy_phone = request.form['delivery_boy_phone']
    delivery_boy_pass = request.form['delivery_boy_pass']

    catfilename = request.files['delivery_boy_image'].filename
    if catfilename == "":
        client = boto3.client('dynamodb', region_name=region_name, aws_access_key_id=aws_access_key_id,
                              aws_secret_access_key=aws_secret_access_key)
        client.update_item(
            TableName='TryonceDeliveryBoy-tloujol3mvdsveoacsa5zlf5fa-tryprod',
            Key={'id': {"S": id}},
            UpdateExpression="set area_id=:v, delivery_boy_name=:s,delivery_boy_phone=:a,delivery_boy_pass=:b ,updatedAt=:u",
            ExpressionAttributeValues={
                ':v': {"S": area_id},
                ':s': {"S": delivery_boy_name},
                ':a': {"S": delivery_boy_phone},
                ':b': {"S": delivery_boy_pass},
                ':u': {"S": datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%fZ")}
            },
            ReturnValues="UPDATED_NEW",
        )
        return redirect(url_for("deliveryboyview"))

    else:
        catfilename = id + catfilename
        status = upload_file(delivery_boy_image, catfilename, bucket)

        if status:
            client = boto3.client('dynamodb', region_name=region_name, aws_access_key_id=aws_access_key_id,
                                  aws_secret_access_key=aws_secret_access_key)
            response = client.update_item(
                TableName='TryonceDeliveryBoy-tloujol3mvdsveoacsa5zlf5fa-tryprod',
                Key={'id': {"S": id}},
                UpdateExpression="set area_id=:v, delivery_boy_name=:s,delivery_boy_phone=:a,delivery_boy_pass=:b,delivery_boy_image=:c ,updatedAt=:u",
                ExpressionAttributeValues={
                    ':v': {"S": area_id},
                    ':s': {"S": delivery_boy_name},
                    ':a': {"S": delivery_boy_phone},
                    ':b': {"S": delivery_boy_pass},
                    ':c': {"S": catfilename},
                    ':u': {"S": datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%fZ")}

                },
                ReturnValues="UPDATED_NEW",
            )

            return redirect(url_for("deliveryboyview"))
        else:
            return redirect(url_for("deliveryboyedit"))


@app.route('/deliveryboydelete/<cid>', methods=['GET', 'POST'])
def deliveryboydelete(cid):
    client = boto3.client('dynamodb', region_name=region_name, aws_access_key_id=aws_access_key_id,
                          aws_secret_access_key=aws_secret_access_key)

    client.delete_item(
        TableName='TryonceDeliveryBoy-tloujol3mvdsveoacsa5zlf5fa-tryprod',
        Key={'id': {"S": cid}}
    )
    return redirect(url_for("deliveryboyview"))


@app.route('/usersview', methods=['GET', 'POST'])
def usersview():
    client = boto3.client('dynamodb', region_name=region_name, aws_access_key_id=aws_access_key_id,
                          aws_secret_access_key=aws_secret_access_key)
    data2 = client.scan(
        TableName='TryonceUserAddresses-tloujol3mvdsveoacsa5zlf5fa-tryprod'
    )
    data = client.scan(
        TableName='TryonceUsers-tloujol3mvdsveoacsa5zlf5fa-tryprod'
    )

    return render_template("usersview.html", active10='active', data=data["Items"], data2=data2["Items"])


@app.route('/usersdelete/<cid>', methods=['GET', 'POST'])
def usersdelete(cid):
    client = boto3.client('dynamodb', region_name=region_name, aws_access_key_id=aws_access_key_id,
                          aws_secret_access_key=aws_secret_access_key)

    client.delete_item(
        TableName='TryonceUsers-tloujol3mvdsveoacsa5zlf5fa-tryprod',
        Key={'id': {"S": cid}}
    )
    return redirect(url_for("usersview"))


@app.route('/areaview', methods=['GET', 'POST'])
def areaview():
    client = boto3.client('dynamodb', region_name=region_name, aws_access_key_id=aws_access_key_id,
                          aws_secret_access_key=aws_secret_access_key)
    area = client.scan(
        TableName='TryonceAreas-tloujol3mvdsveoacsa5zlf5fa-tryprod'
    )

    city = client.scan(
        TableName='TryonceCities-tloujol3mvdsveoacsa5zlf5fa-tryprod'
    )

    return render_template("areaview.html", active7='active', area=area["Items"], city=city["Items"])


@app.route('/addarea', methods=['GET', 'POST'])
def addarea():
    if request.method == "GET":
        client = boto3.client('dynamodb', region_name=region_name, aws_access_key_id=aws_access_key_id,
                              aws_secret_access_key=aws_secret_access_key)
        response = client.scan(
            TableName='TryonceCities-tloujol3mvdsveoacsa5zlf5fa-tryprod'
        )

        return render_template('addarea.html', active7='active', city=response["Items"])
    if request.method == "POST":
        area_id = ''.join(random.choices(string.digits, k=9))
        area_name = request.form['area_name']
        city_id = request.form['city_id']
        delivery_charge = request.form['delivery_charge']

        client = boto3.client('dynamodb', region_name=region_name, aws_access_key_id=aws_access_key_id,
                              aws_secret_access_key=aws_secret_access_key)
        response = client.put_item(
            TableName='TryonceAreas-tloujol3mvdsveoacsa5zlf5fa-tryprod',
            Item={
                'id': {"S": area_id},
                'area_id': {"S": area_id},
                'area_name': {"S": area_name},
                'city_id': {"S": city_id},
                'cod': {"S": "no"},
                'delivery_charge': {"S": delivery_charge},
                'createdAt': {"S": datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%fZ")},
                'updatedAt': {"S": datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%fZ")}
            })


        return redirect(url_for("areaview"))


@app.route('/addcity', methods=['GET', 'POST'])
def addcity():
    if request.method == "GET":
        return render_template('addcity.html', active7='active')
    if request.method == "POST":
        city_id = ''.join(random.choices(string.digits, k=9))
        device_id = ''.join(random.choices(string.digits, k=9))
        cityadmin_id = ''.join(random.choices(string.digits, k=9))
        city_name = request.form['city_name']
        city_pincode = request.form['city_pincode']
        city_image = request.files['city_image']
        catfilename = request.files['city_image'].filename
        catfilename = city_id + catfilename
        status = upload_file(city_image, catfilename, bucket)

        if status:
            client = boto3.client('dynamodb', region_name=region_name, aws_access_key_id=aws_access_key_id,
                                  aws_secret_access_key=aws_secret_access_key)
            response = client.put_item(
                TableName='TryonceCities-tloujol3mvdsveoacsa5zlf5fa-tryprod',
                Item={
                    'id': {"S": city_id},
                    'city_id': {"S": city_id},
                    'device_id': {"S": device_id},
                    'city_pincode': {"S": city_pincode},
                    'city_name': {"S": city_name},
                    'city_image': {"S": catfilename},
                    'cityadmin_id': {"N": cityadmin_id},
                    'cityadmin_phone' : {"S": "0000000000"},
                    'cityadmin_image' :  {"S": "apple.jpg"},
                    'cityadmin_name': {"S": "xxxx"},
                    'cityadmin_address': {"S": "xxxx"},
                    'cityadmin_pass': {"S": admin_password},
                    'cityadmin_email': {"S": admin_username},
                    'lat': {"S": "1.1"},
                    'lng': {"S": "1.1"},
                    '_version': {"S": str(1)},
                    'createdAt': {"S": datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%fZ")},
                    'updatedAt': {"S": datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%fZ")}
                })

            return redirect(url_for("areaview"))
        else:
            return redirect(url_for("addcity"))


@app.route('/citydelete/<cid>', methods=['GET', 'POST'])
def citydelete(cid):
    client = boto3.client('dynamodb', region_name=region_name, aws_access_key_id=aws_access_key_id,
                          aws_secret_access_key=aws_secret_access_key)

    response = client.get_item(
        TableName='TryonceCities-tloujol3mvdsveoacsa5zlf5fa-tryprod',
        Key={'id': {"S": cid}}
    )
    response2 = client.delete_item(
        TableName='TryonceCities-tloujol3mvdsveoacsa5zlf5fa-tryprod',
        Key={'id': {"S": cid}}
    )
    delete_from_s3(bucket, response["Item"]["city_image"]["S"])
    return redirect(url_for("areaview"))


@app.route('/areadelete/<cid>', methods=['GET', 'POST'])
def areadelete(cid):
    client = boto3.client('dynamodb', region_name=region_name, aws_access_key_id=aws_access_key_id,
                          aws_secret_access_key=aws_secret_access_key)
    client.delete_item(
        TableName='TryonceAreas-tloujol3mvdsveoacsa5zlf5fa-tryprod',
        Key={'id': {"S": cid}}
    )
    return redirect(url_for("areaview"))


@app.route('/areaedit/<cid>', methods=['GET', 'POST'])
def areaedit(cid):
    client = boto3.client('dynamodb', region_name=region_name, aws_access_key_id=aws_access_key_id,
                          aws_secret_access_key=aws_secret_access_key)

    area = client.get_item(
        TableName='TryonceAreas-tloujol3mvdsveoacsa5zlf5fa-tryprod',
        Key={'id': {"S": cid}}
    )

    city = client.scan(
        TableName='TryonceCities-tloujol3mvdsveoacsa5zlf5fa-tryprod'
    )

    return render_template("editarea.html", active7='active', area=area["Item"], city=city["Items"])

@app.route('/editarea', methods=['GET', 'POST'])
def editarea():
    area_name = request.form['area_name']
    area_id = request.form['area_id']
    city_id = request.form['city_id']
    delivery_charge = request.form['delivery_charge']
    print(area_id,area_name,city_id)
    client = boto3.client('dynamodb', region_name=region_name, aws_access_key_id=aws_access_key_id,
                          aws_secret_access_key=aws_secret_access_key)
    client.update_item(
        TableName='TryonceAreas-tloujol3mvdsveoacsa5zlf5fa-tryprod',
        Key={'id': {"S": area_id}},
        UpdateExpression="set area_name=:v, city_id=:s, delivery_charge=:c, updatedAt=:u",
        ExpressionAttributeValues={
            ':v': {"S": area_name},
            ':s': {"S": city_id},
            ':c': {"S": delivery_charge},
            ':u': {"S": datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%fZ")}
        },
        ReturnValues="UPDATED_NEW",
    )
    return redirect(url_for("areaview"))


@app.route('/cityedit/<cid>', methods=['GET', 'POST'])
def cityedit(cid):
    client = boto3.client('dynamodb', region_name=region_name, aws_access_key_id=aws_access_key_id,
                          aws_secret_access_key=aws_secret_access_key)

    city = client.get_item(
        TableName='TryonceCities-tloujol3mvdsveoacsa5zlf5fa-tryprod',
        Key={'id': {"S": cid}}
    )
    return render_template("editcity.html", active7='active',  city=city["Item"])


@app.route('/editcity', methods=['GET', 'POST'])
def editcity():
    city_name = request.form['city_name']
    city_pincode = request.form['city_pincode']
    city_image = request.files['city_image']
    city_id = request.form['city_id']
    catfilename = request.files['city_image'].filename
    if catfilename == "":
        client = boto3.client('dynamodb', region_name=region_name, aws_access_key_id=aws_access_key_id,
                              aws_secret_access_key=aws_secret_access_key)
        client.update_item(
            TableName='TryonceCities-tloujol3mvdsveoacsa5zlf5fa-tryprod',
            Key={'id': {"S": city_id}},
            UpdateExpression="set city_name=:v, city_pincode=:s, updatedAt=:u",
            ExpressionAttributeValues={
                ':v': {"S": city_name},
                ':s': {"S": city_pincode},
                ':u': {"S": datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%fZ")}
            },
            ReturnValues="UPDATED_NEW",
        )
        return redirect(url_for("areaview"))

    else:
        catfilename = city_id + catfilename
        status = upload_file(city_image, catfilename, bucket)

        if status:
            client = boto3.client('dynamodb', region_name=region_name, aws_access_key_id=aws_access_key_id,
                                  aws_secret_access_key=aws_secret_access_key)
            response = client.update_item(
                TableName='TryonceCities-tloujol3mvdsveoacsa5zlf5fa-tryprod',
                Key={'id': {"S": city_id}},
                UpdateExpression="set city_name=:v, city_image=:a, city_pincode=:c, updatedAt=:u",
                ExpressionAttributeValues={
                    ':v': {"S": city_name},
                    ':a': {"S": catfilename},
                    ':c': {"S": city_pincode},
                    ':u': {"S": datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%fZ")}

                },
                ReturnValues="UPDATED_NEW",
            )

            return redirect(url_for("areaview"))
        else:
            return redirect(url_for("cityedit"))



# -----------------------------------------------------------------------------------------------------------SubCategory

@app.route('/subcatview', methods=['GET', 'POST'])
def subcatview():
    client = boto3.client('dynamodb', region_name=region_name, aws_access_key_id=aws_access_key_id,
                          aws_secret_access_key=aws_secret_access_key)
    response = client.scan(
        TableName='TryonceSubCategories-tloujol3mvdsveoacsa5zlf5fa-tryprod'
    )
    # print(response["Items"][1]["category_name"])
    return render_template("subcatview.html", active1='active', data=response["Items"])


@app.route('/subcatedit/<cid>', methods=['GET', 'POST'])
def subcatedit(cid):
    client = boto3.client('dynamodb', region_name=region_name, aws_access_key_id=aws_access_key_id,
                          aws_secret_access_key=aws_secret_access_key)

    response = client.get_item(
        TableName='TryonceSubCategories-tloujol3mvdsveoacsa5zlf5fa-tryprod',
        Key={'id': {"S": cid}}
    )

    response2 = client.get_item(
        TableName='TryonceCategories-tloujol3mvdsveoacsa5zlf5fa-tryprod',
        Key={'id': {"S": response["Item"]["category_id"]["S"]}}
    )
    response3 = client.scan(
        TableName='TryonceCategories-tloujol3mvdsveoacsa5zlf5fa-tryprod'
    )

    return render_template("editsubcategory.html", active1='active', data=response["Item"], datacat=response2["Item"],
                           data3=response3["Items"])


@app.route('/editsubcategory', methods=['GET', 'POST'])
def editsubcat():
    subcat_id = request.form['subcat_id']
    category_id = request.form['category_id']
    subcat_name = request.form['subcat_name']
    catimage = request.files['subcat_image']
    catfilename = request.files['subcat_image'].filename
    if catfilename == "":
        client = boto3.client('dynamodb', region_name=region_name, aws_access_key_id=aws_access_key_id,
                              aws_secret_access_key=aws_secret_access_key)
        response = client.update_item(
            TableName='TryonceSubCategories-tloujol3mvdsveoacsa5zlf5fa-tryprod',
            Key={'id': {"S": subcat_id}},
            UpdateExpression="set subcat_name=:v, tryonceSubCategoriesCategoryId=:s, category_id=:c, updatedAt=:u",
            ExpressionAttributeValues={
                ':v': {"S": subcat_name},
                ':c': {"S": category_id},
                ':s': {"S": category_id},
                ':u': {"S": datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%fZ")}
            },
            ReturnValues="UPDATED_NEW",
        )
        return redirect(url_for("subcatview"))

    else:
        catfilename = subcat_id + catfilename
        status = upload_file(catimage, catfilename, bucket)

        if status:
            client = boto3.client('dynamodb', region_name=region_name, aws_access_key_id=aws_access_key_id,
                                  aws_secret_access_key=aws_secret_access_key)
            response = client.update_item(
                TableName='TryonceSubCategories-tloujol3mvdsveoacsa5zlf5fa-tryprod',
                Key={'id': {"S": subcat_id}},
                UpdateExpression="set subcat_name=:v, tryonceSubCategoriesCategoryId=:s, subcat_image=:a, category_id=:c, updatedAt=:u",
                ExpressionAttributeValues={
                    ':v': {"S": subcat_name},
                    ':a': {"S": catfilename},
                    ':c': {"S": category_id},
                    ':s': {"S": category_id},
                    ':u': {"S": datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%fZ")}

                },
                ReturnValues="UPDATED_NEW",
            )

            print(response)
            return redirect(url_for("subcatview"))
        else:
            return redirect(url_for("subcatedit"))


@app.route('/subcatdelete/<cid>', methods=['GET', 'POST'])
def subcatdelete(cid):
    client = boto3.client('dynamodb', region_name=region_name, aws_access_key_id=aws_access_key_id,
                          aws_secret_access_key=aws_secret_access_key)

    response = client.get_item(
        TableName='TryonceSubCategories-tloujol3mvdsveoacsa5zlf5fa-tryprod',
        Key={'id': {"S": cid}}
    )
    response2 = client.delete_item(
        TableName='TryonceSubCategories-tloujol3mvdsveoacsa5zlf5fa-tryprod',
        Key={'id': {"S": cid}}
    )
    print(response["Item"]["subcat_image"]["S"])
    delete_from_s3(bucket, response["Item"]["subcat_image"]["S"])
    return redirect(url_for("subcatview"))


@app.route('/addsubcategory', methods=['GET', 'POST'])
def addsubcat():
    if request.method == "GET":
        client = boto3.client('dynamodb', region_name=region_name, aws_access_key_id=aws_access_key_id,
                              aws_secret_access_key=aws_secret_access_key)
        response = client.scan(
            TableName='TryonceCategories-tloujol3mvdsveoacsa5zlf5fa-tryprod'
        )

        return render_template('addsubcategory.html', active1='active', data=response["Items"])
    if request.method == "POST":
        subcat_id = request.form['subcat_id']
        subcat_name = request.form['subcat_name']
        subcat_image = request.files['subcat_image']
        category_id = request.form['category_id']
        catfilename = request.files['subcat_image'].filename
        catfilename = subcat_id + catfilename
        print(catfilename, category_id, subcat_name)

        print(current_user.fullname)  # need to get current user adminid
        # replace below line with s3 bucket upload
        status = upload_file(subcat_image, catfilename, bucket)
        # status will be true if success
        if status:
            client = boto3.client('dynamodb', region_name=region_name, aws_access_key_id=aws_access_key_id,
                                  aws_secret_access_key=aws_secret_access_key)
            response = client.put_item(
                TableName='TryonceSubCategories-tloujol3mvdsveoacsa5zlf5fa-tryprod',
                Item={
                    'id': {"S": subcat_id},
                    'subcat_id': {"S": subcat_id},
                    'category_id': {"S": category_id},
                    'tryonceSubCategoriesCategoryId': {"S": category_id},
                    'subcat_name': {"S": subcat_name},
                    # 'cityadmin_id': {"S": str(16)},
                    'subcat_image': {"S": catfilename},
                    '_version': {"S": str(1)},
                    'createdAt': {"S": datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%fZ")},
                    'updatedAt': {"S": datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%fZ")}
                })

            return redirect(url_for("subcatview"))
        else:
            return redirect(url_for("addsubcat"))


# -------------------------------------------------------------------------------- ----------------- --  All Categories


@app.route('/catview', methods=['GET', 'POST'])
def catview():
    client = boto3.client('dynamodb', region_name=region_name, aws_access_key_id=aws_access_key_id,
                          aws_secret_access_key=aws_secret_access_key)
    response = client.scan(
        TableName='TryonceCategories-tloujol3mvdsveoacsa5zlf5fa-tryprod'
    )

    return render_template("catview.html", active='active', data=response["Items"])


@app.route('/catedit/<cid>', methods=['GET', 'POST'])
def catedit(cid):
    client = boto3.client('dynamodb', region_name=region_name, aws_access_key_id=aws_access_key_id,
                          aws_secret_access_key=aws_secret_access_key)

    response = client.get_item(
        TableName='TryonceCategories-tloujol3mvdsveoacsa5zlf5fa-tryprod',
        Key={'id': {"S": cid}}
    )
    TABLE_NAME = "TryonceDailyEssentials-tloujol3mvdsveoacsa5zlf5fa-tryprod"
    table = dynamodb_resource.Table(TABLE_NAME)
    response3 = table.query(IndexName='category_id-index', KeyConditionExpression=Key("category_id").eq(cid))

    TABLE_NAME = "TryonceTopDeals-tloujol3mvdsveoacsa5zlf5fa-tryprod"
    table = dynamodb_resource.Table(TABLE_NAME)
    response4 = table.query(IndexName='category_id-index', KeyConditionExpression=Key("category_id").eq(cid))


    if response3['Count'] > 0:
        dailyessential = 'checked'
    else:
        dailyessential = ''

    if response4['Count'] > 0:
        topdeal = 'checked'
    else:
        topdeal = ''

    print("dailyessentials", dailyessential)
    print("topdeals", topdeal)
    return render_template("editcategory.html", active='active', data=response["Item"],daily=dailyessential,topdeal=topdeal)


@app.route('/editcategory', methods=['GET', 'POST'])
def editcat():
    try:
        topdeals = request.form['topdeals']
    except:
        topdeals = "off"
    try:
        dailye = request.form['dailye']
    except:
        dailye = "off"

    catname = request.form['catname']
    category_id = request.form['catid']
    catimage = request.files['catimage']
    catfilename = request.files['catimage'].filename
    if catfilename == "":
        client = boto3.client('dynamodb', region_name=region_name, aws_access_key_id=aws_access_key_id,
                              aws_secret_access_key=aws_secret_access_key)
        response = client.update_item(
            TableName='TryonceCategories-tloujol3mvdsveoacsa5zlf5fa-tryprod',
            Key={'id': {"S": category_id}},
            UpdateExpression="set category_name=:v, updatedAt=:u",
            ExpressionAttributeValues={
                ':v': {"S": catname},
                ':u': {"S": datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%fZ")}
            },
            ReturnValues="UPDATED_NEW",
        )

        if topdeals == "on":

            TABLE_NAME = "TryonceTopDeals-tloujol3mvdsveoacsa5zlf5fa-tryprod"
            table = dynamodb_resource.Table(TABLE_NAME)
            response41 = table.query(IndexName='category_id-index', KeyConditionExpression=Key("category_id").eq(category_id))


            if response41['Count'] > 0:
                topdeal = 'yes'
            else:
                topdeal = 'no'

            if topdeal == 'no':

                randid = ''.join(random.choices(string.digits, k=9))

                response = client.put_item(
                    TableName='TryonceTopDeals-tloujol3mvdsveoacsa5zlf5fa-tryprod',
                    Item={
                        'id': {"S": randid},
                        'category_id': {"S": category_id},
                        'cityadmin_id': {"S": str(cityadmin_id)},
                        'assign_id': {"S": category_id},
                        'homecat_id': {"S": randid},
                        'home': {"S": str(1)},
                        'createdAt': {"S": datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%fZ")},
                        'updatedAt': {"S": datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%fZ")}
                    })
        if dailye == "on":
            TABLE_NAME = "TryonceDailyEssentials-tloujol3mvdsveoacsa5zlf5fa-tryprod"
            table = dynamodb_resource.Table(TABLE_NAME)
            response31 = table.query(IndexName='category_id-index', KeyConditionExpression=Key("category_id").eq(category_id))

            if response31['Count'] > 0:
                daily = 'yes'
            else:
                daily = 'no'
            if daily == 'no':

                randid = ''.join(random.choices(string.digits, k=9))

                client.put_item(
                    TableName='TryonceDailyEssentials-tloujol3mvdsveoacsa5zlf5fa-tryprod',
                    Item={
                        'id': {"S": randid},
                        'category_id': {"S": category_id},
                        'cityadmin_id': {"S": str(cityadmin_id)},
                        'assign_id': {"S": category_id},
                        'homecat_id': {"S": randid},
                        'home': {"S": str(1)},
                        'createdAt': {"S": datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%fZ")},
                        'updatedAt': {"S": datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%fZ")}
                    })

        return redirect(url_for("catview"))

    else:
        catfilename = category_id + catfilename
        status = upload_file(catimage, catfilename, bucket)

        if status:
            client = boto3.client('dynamodb', region_name=region_name, aws_access_key_id=aws_access_key_id,
                                  aws_secret_access_key=aws_secret_access_key)
            response = client.update_item(
                TableName='TryonceCategories-tloujol3mvdsveoacsa5zlf5fa-tryprod',
                Key={'id': {"S": category_id}},
                UpdateExpression="set category_name=:v, category_image=:a, updatedAt=:u",
                ExpressionAttributeValues={
                    ':v': {"S": catname},
                    ':a': {"S": catfilename},
                    ':u': {"S": datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%fZ")}

                },
                ReturnValues="UPDATED_NEW",
            )


            if topdeals == "on":

                TABLE_NAME = "TryonceTopDeals-tloujol3mvdsveoacsa5zlf5fa-tryprod"
                table = dynamodb_resource.Table(TABLE_NAME)
                response41 = table.query(IndexName='category_id-index',
                                         KeyConditionExpression=Key("category_id").eq(category_id))

                if response41['Count'] > 0:
                    topdeal = 'yes'
                else:
                    topdeal = 'no'

                if topdeal == 'no':

                    randid = ''.join(random.choices(string.digits, k=9))

                    response = client.put_item(
                        TableName='TryonceTopDeals-tloujol3mvdsveoacsa5zlf5fa-tryprod',
                        Item={
                            'id': {"S": randid},
                            'category_id': {"S": category_id},
                            'cityadmin_id': {"S": str(cityadmin_id)},
                            'assign_id': {"S": category_id},
                            'homecat_id': {"S": randid},
                            'home': {"S": str(1)},
                            'createdAt': {"S": datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%fZ")},
                            'updatedAt': {"S": datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%fZ")}
                        })
            if dailye == "on":

                TABLE_NAME = "TryonceDailyEssentials-tloujol3mvdsveoacsa5zlf5fa-tryprod"
                table = dynamodb_resource.Table(TABLE_NAME)
                response31 = table.query(IndexName='category_id-index',
                                         KeyConditionExpression=Key("category_id").eq(category_id))

                if response31['Count'] > 0:
                    daily = 'yes'
                else:
                    daily = 'no'
                if daily == 'no':

                    randid = ''.join(random.choices(string.digits, k=9))

                    client.put_item(
                        TableName='TryonceDailyEssentials-tloujol3mvdsveoacsa5zlf5fa-tryprod',
                        Item={
                            'id': {"S": randid},
                            'category_id': {"S": category_id},
                            'cityadmin_id': {"S": str(cityadmin_id)},
                            'assign_id': {"S": category_id},
                            'homecat_id': {"S": randid},
                            'home': {"S": str(1)},
                            'createdAt': {"S": datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%fZ")},
                            'updatedAt': {"S": datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%fZ")}
                        })
            return redirect(url_for("catview"))
        else:
            return redirect(url_for("catedit"))


@app.route('/catdelete/<cid>', methods=['GET', 'POST'])
def catdelete(cid):
    client = boto3.client('dynamodb', region_name=region_name, aws_access_key_id=aws_access_key_id,
                          aws_secret_access_key=aws_secret_access_key)

    response = client.get_item(
        TableName='TryonceCategories-tloujol3mvdsveoacsa5zlf5fa-tryprod',
        Key={'id': {"S": cid}}
    )
    response2 = client.delete_item(
        TableName='TryonceCategories-tloujol3mvdsveoacsa5zlf5fa-tryprod',
        Key={'id': {"S": cid}}
    )
    print(response["Item"]["category_image"]["S"])
    delete_from_s3(bucket, response["Item"]["category_image"]["S"])
    return redirect(url_for("catview"))


def delete_from_s3(bucket, model):
    try:
        model = str(objectpath)+str(model)
        s3 = boto3.client('s3', region_name=region_name, aws_access_key_id=aws_access_key_id,
                          aws_secret_access_key=aws_secret_access_key
                          )

        s3.delete_object(Bucket=bucket, Key=model)
        return True
    except Exception as ex:
        print(str(ex))
        return False


@app.route('/addcategory', methods=['GET', 'POST'])
def addcat():
    if request.method == "GET":
        return render_template('addcategory.html', active='active')
    if request.method == "POST":
        try:
            topdeals = request.form['topdeals']
        except:
            topdeals = "off"
        try:
            dailye = request.form['dailye']
        except:
            dailye = "off"

        print(topdeals, dailye)

        catname = request.form['catname']
        category_id = request.form['catid']
        catimage = request.files['catimage']
        catfilename = request.files['catimage'].filename
        catfilename = category_id + catfilename
        print(catfilename, category_id, catname)
        print(current_user.fullname)  # need to get current user adminid
        # replace below line with s3 bucket upload

        status = upload_file(catimage, catfilename, bucket)
        # status will be true if success
        if status:
            client = boto3.client('dynamodb', region_name=region_name, aws_access_key_id=aws_access_key_id,
                                  aws_secret_access_key=aws_secret_access_key)
            response = client.put_item(
                TableName='TryonceCategories-tloujol3mvdsveoacsa5zlf5fa-tryprod',
                Item={
                    'id': {"S": category_id},
                    'category_id': {"S": category_id},
                    'category_name': {"S": catname},
                    'cityadmin_id': {"S": str(cityadmin_id)},
                    'category_image': {"S": catfilename},
                    '_version': {"S": str(1)},
                    'createdAt': {"S": datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%fZ")},
                    'updatedAt': {"S": datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%fZ")}
                })

            if topdeals == "on":
                randid = ''.join(random.choices(string.digits, k=9))

                response = client.put_item(
                    TableName='TryonceTopDeals-tloujol3mvdsveoacsa5zlf5fa-tryprod',
                    Item={
                        'id': {"S": randid},
                        'category_id': {"S": category_id},
                        'cityadmin_id': {"S": str(cityadmin_id)},
                        'assign_id': {"S": category_id},
                        'homecat_id': {"S": randid},
                        'home': {"S": str(1)},
                        'createdAt': {"S": datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%fZ")},
                        'updatedAt': {"S": datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%fZ")}
                    })
            if dailye == "on":
                randid = ''.join(random.choices(string.digits, k=9))

                client.put_item(
                    TableName='TryonceDailyEssentials-tloujol3mvdsveoacsa5zlf5fa-tryprod',
                    Item={
                        'id': {"S": randid},
                        'category_id': {"S": category_id},
                        'cityadmin_id': {"S": str(cityadmin_id)},
                        'assign_id': {"S": category_id},
                        'homecat_id': {"S": randid},
                        'home': {"S": str(1)},
                        'createdAt': {"S": datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%fZ")},
                        'updatedAt': {"S": datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%fZ")}
                    })

            return redirect(url_for("catview"))
        else:
            return redirect(url_for("addcat"))


def upload_file(file, file_name, bucket, object_name=None):
    """Upload a file to an S3 bucket

    :param file_name: File to upload
    :param bucket: Bucket to upload to
    :param object_name: S3 object name. If not specified then file_name is used
    :return: True if file was uploaded, else False
    """

    # If S3 object_name was not specified, use file_name
    if object_name is None:
        object_name = os.path.basename(file_name)
        print(object_name)

    s3_client = boto3.client('s3', aws_access_key_id=aws_access_key_id,
                             aws_secret_access_key=aws_secret_access_key)
    try:
        print(object_name)
        object_name = str(objectpath)+str(object_name)
        response = s3_client.upload_fileobj(
            file, bucket, object_name, ExtraArgs={'ACL': 'public-read'})
    except ClientError as e:
        logging.error(e)
        return False
    return True


# --------------------------------------------------------------------------------- --------- ---- ----- - Product Code


@app.route('/productview', methods=['GET', 'POST'])
def productview():
    client = boto3.client('dynamodb', region_name=region_name, aws_access_key_id=aws_access_key_id,
                          aws_secret_access_key=aws_secret_access_key)
    response = client.scan(
        TableName='TryonceProducts-tloujol3mvdsveoacsa5zlf5fa-tryprod'
    )
    return render_template("productview.html", active2='active', data=response["Items"])


@app.route('/productdelete/<cid>', methods=['GET', 'POST'])
def productdelete(cid):
    client = boto3.client('dynamodb', region_name=region_name, aws_access_key_id=aws_access_key_id,
                          aws_secret_access_key=aws_secret_access_key)

    response = client.get_item(
        TableName='TryonceProducts-tloujol3mvdsveoacsa5zlf5fa-tryprod',
        Key={'id': {"S": cid}}
    )
    response2 = client.delete_item(
        TableName='TryonceProducts-tloujol3mvdsveoacsa5zlf5fa-tryprod',
        Key={'id': {"S": cid}}
    )
    try:
        tryonceProductsSubscriptionofthedayId = response[
            "Item"]["tryonceProductsSubscriptionofthedayId"]["S"]
        response3 = client.delete_item(
            TableName='TryonceSubscriptionsoftheday-tloujol3mvdsveoacsa5zlf5fa-tryprod',
            Key={'id': {"S": tryonceProductsSubscriptionofthedayId}}
        )
    except:
        pass
    # print(response["Item"]["category_image"]["S"])
    delete_from_s3(bucket, response["Item"]["product_image"]["S"])
    return redirect(url_for("productview"))


@app.route('/productedit/<cid>', methods=['GET', 'POST'])
def productedit(cid):
    client = boto3.client('dynamodb', region_name=region_name, aws_access_key_id=aws_access_key_id,
                          aws_secret_access_key=aws_secret_access_key)

    response = client.get_item(
        TableName='TryonceProducts-tloujol3mvdsveoacsa5zlf5fa-tryprod',
        Key={'id': {"S": cid}}
    )
    response2 = client.scan(
        TableName='TryonceSubCategories-tloujol3mvdsveoacsa5zlf5fa-tryprod'
    )
    response3 = client.scan(
        TableName='TryonceCategories-tloujol3mvdsveoacsa5zlf5fa-tryprod'
    )
    try:
        tryonceProductsSubscriptionofthedayId = response[
            "Item"]["tryonceProductsSubscriptionofthedayId"]["S"]
        response4 = client.get_item(
            TableName='TryonceSubscriptionsoftheday-tloujol3mvdsveoacsa5zlf5fa-tryprod',
            Key={'id': {"S": tryonceProductsSubscriptionofthedayId}}
        )
        return render_template("editproduct.html", active2='active', data=response["Item"], data2=response2["Items"],
                               data3=response3["Items"], data4=response4["Item"])
    except:
        return render_template("editproduct.html", active2='active', data=response["Item"], data2=response2["Items"],
                               data3=response3["Items"])


@app.route('/editproduct', methods=['GET', 'POST'])
def editproduct():
    try:
        suboday = request.form['suboday']
    except:
        suboday = "off"

    print(suboday)

    product_name = request.form['product_name']
    # product_image = request.form['product_image'] #catimage is product_image
    product_id = request.form['product_id']
    id = request.form['product_id']
    description = request.form['description']
    tryonceProductsSubcategoryId = request.form['subcat_id']
    subcat_id = request.form['subcat_id']
    unit = request.form['unit']
    subscription_price = request.form['subscription_price']
    stock = request.form['stock']
    qty = request.form['qty']
    price = request.form['price']
    mrp = request.form['mrp']
    membership_price = request.form['subscription_price']

    catimage = request.files['product_image']
    catfilename = request.files['product_image'].filename
    # catfilename = product_id + catfilename

    if catfilename == "":
        client = boto3.client('dynamodb', region_name=region_name, aws_access_key_id=aws_access_key_id,
                              aws_secret_access_key=aws_secret_access_key)
        subodayid = ""
        if suboday == "on":
            count = request.form["count"]
            abc = ""
            try:
                abc = request.form["subodayid"]
            except:
                pass
            if abc:
                subodayid = request.form["subodayid"]
                response = client.update_item(
                    TableName='TryonceSubscriptionsoftheday-tloujol3mvdsveoacsa5zlf5fa-tryprod',
                    Key={'id': {"S": subodayid}},
                    UpdateExpression="set #cou=:a, updatedAt=:u",
                    ExpressionAttributeValues={
                        ':a': {"N": count},
                        ':u': {"S": datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%fZ")}
                    },
                    ExpressionAttributeNames={
                        "#cou": "count"
                    },
                    ReturnValues="UPDATED_NEW",
                )
            else:
                subodayid = ''.join(random.choices(string.digits, k=9))
                response = client.put_item(
                    TableName='TryonceSubscriptionsoftheday-tloujol3mvdsveoacsa5zlf5fa-tryprod',
                    Item={
                        'id': {"S": subodayid},
                        'product_id': {"S": product_id},
                        'cityadmin_id': {"S": str(cityadmin_id)},
                        'count': {"N": count},
                        'createdAt': {"S": datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%fZ")},
                        'updatedAt': {"S": datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%fZ")}
                    })
            print(subodayid)
            response = client.update_item(
                TableName='TryonceProducts-tloujol3mvdsveoacsa5zlf5fa-tryprod',
                Key={'id': {"S": id}},
                UpdateExpression="set product_name=:v,tryonceProductsSubscriptionofthedayId=:y, description=:a, tryonceProductsSubcategoryId=:b, subcat_id=:c, #uni=:d,subscription_price=:e,stock=:f,qty=:g,price=:h,mrp=:i,membership_price=:j, updatedAt=:u",
                ExpressionAttributeValues={
                    ':v': {"S": product_name},
                    ':a': {"S": description},
                    ':b': {"S": tryonceProductsSubcategoryId},
                    ':c': {"S": subcat_id},
                    ':d': {"S": unit},
                    ':e': {"S": subscription_price},
                    ':f': {"S": stock},
                    ':g': {"S": qty},
                    ':h': {"S": price},
                    ':i': {"S": mrp},
                    ':j': {"S": membership_price},
                    ':y': {"S": subodayid},
                    ':u': {"S": datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%fZ")}
                },
                ExpressionAttributeNames={
                    "#uni": "unit"
                },
                ReturnValues="UPDATED_NEW",
            )
            return redirect(url_for("productview"))
        else:
            subodayid = ""
            try:
                subodayid = request.form["subodayid"]
            except:
                pass
            if subodayid:
                subodayid = request.form["subodayid"]
                response9 = client.delete_item(
                    TableName='TryonceSubscriptionsoftheday-tloujol3mvdsveoacsa5zlf5fa-tryprod',
                    Key={'id': {"S": subodayid}}
                )
            response = client.update_item(
                TableName='TryonceProducts-tloujol3mvdsveoacsa5zlf5fa-tryprod',
                Key={'id': {"S": id}},
                UpdateExpression="set product_name=:v,tryonceProductsSubscriptionofthedayId=:y, description=:a, tryonceProductsSubcategoryId=:b, subcat_id=:c, #uni=:d,subscription_price=:e,stock=:f,qty=:g,price=:h,mrp=:i,membership_price=:j, updatedAt=:u",
                ExpressionAttributeValues={
                    ':v': {"S": product_name},
                    ':a': {"S": description},
                    ':b': {"S": tryonceProductsSubcategoryId},
                    ':c': {"S": subcat_id},
                    ':d': {"S": unit},
                    ':e': {"S": subscription_price},
                    ':f': {"S": stock},
                    ':g': {"S": qty},
                    ':h': {"S": price},
                    ':i': {"S": mrp},
                    ':j': {"S": membership_price},
                    ':y': {"S": " "},
                    ':u': {"S": datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%fZ")}
                },
                ExpressionAttributeNames={
                    "#uni": "unit"
                },
                ReturnValues="UPDATED_NEW",
            )
            return redirect(url_for("productview"))

    else:
        catfilename = product_id + catfilename
        status = upload_file(catimage, catfilename, bucket)

        if status:
            client = boto3.client('dynamodb', region_name=region_name, aws_access_key_id=aws_access_key_id,
                                  aws_secret_access_key=aws_secret_access_key)
            subodayid = ""
            if suboday == "on":
                count = request.form["count"]
                abc = ""
                try:
                    abc = request.form["subodayid"]
                except:
                    pass
                if abc:
                    subodayid = request.form["subodayid"]
                    response = client.update_item(
                        TableName='TryonceSubscriptionsoftheday-tloujol3mvdsveoacsa5zlf5fa-tryprod',
                        Key={'id': {"S": subodayid}},
                        UpdateExpression="set #cou=:a, updatedAt=:u",
                        ExpressionAttributeValues={
                            ':a': {"N": count},
                            ':u': {"S": datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%fZ")}
                        },
                        ExpressionAttributeNames={
                            "#cou": "count"
                        },
                        ReturnValues="UPDATED_NEW",
                    )
                else:
                    subodayid = ''.join(random.choices(string.digits, k=9))
                    response = client.put_item(
                        TableName='TryonceSubscriptionsoftheday-tloujol3mvdsveoacsa5zlf5fa-tryprod',
                        Item={
                            'id': {"S": subodayid},
                            'product_id': {"S": product_id},
                            'cityadmin_id': {"S": str(cityadmin_id)},
                            'count': {"N": count},
                            'createdAt': {"S": datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%fZ")},
                            'updatedAt': {"S": datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%fZ")}
                        })
                print(subodayid)
                response = client.update_item(
                    TableName='TryonceProducts-tloujol3mvdsveoacsa5zlf5fa-tryprod',
                    Key={'id': {"S": id}},
                    UpdateExpression="set product_name=:v,tryonceProductsSubscriptionofthedayId=:y, product_image=:z,description=:a, tryonceProductsSubcategoryId=:b, subcat_id=:c, #uni=:d,subscription_price=:e,stock=:f,qty=:g,price=:h,mrp=:i,membership_price=:j, updatedAt=:u",
                    ExpressionAttributeValues={
                        ':v': {"S": product_name},
                        ':a': {"S": description},
                        ':b': {"S": tryonceProductsSubcategoryId},
                        ':c': {"S": subcat_id},
                        ':d': {"S": unit},
                        ':e': {"S": subscription_price},
                        ':f': {"S": stock},
                        ':g': {"S": qty},
                        ':h': {"S": price},
                        ':i': {"S": mrp},
                        ':j': {"S": membership_price},
                        ':y': {"S": subodayid},
                        ':z': {"S": catfilename},
                        ':u': {"S": datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%fZ")}
                    },
                    ExpressionAttributeNames={
                        "#uni": "unit"
                    },
                    ReturnValues="UPDATED_NEW",
                )

                print(response)
                return redirect(url_for("productview"))
            else:
                subodayid = ""
                try:
                    subodayid = request.form["subodayid"]
                except:
                    pass
                if subodayid:
                    subodayid = request.form["subodayid"]
                    response9 = client.delete_item(
                        TableName='TryonceSubscriptionsoftheday-tloujol3mvdsveoacsa5zlf5fa-tryprod',
                        Key={'id': {"S": subodayid}}
                    )
                response = client.update_item(
                    TableName='TryonceProducts-tloujol3mvdsveoacsa5zlf5fa-tryprod',
                    Key={'id': {"S": id}},
                    UpdateExpression="set product_name=:v,tryonceProductsSubscriptionofthedayId=:y, product_image=:z,description=:a, tryonceProductsSubcategoryId=:b, subcat_id=:c, #uni=:d,subscription_price=:e,stock=:f,qty=:g,price=:h,mrp=:i,membership_price=:j, updatedAt=:u",
                    ExpressionAttributeValues={
                        ':v': {"S": product_name},
                        ':a': {"S": description},
                        ':b': {"S": tryonceProductsSubcategoryId},
                        ':c': {"S": subcat_id},
                        ':d': {"S": unit},
                        ':e': {"S": subscription_price},
                        ':f': {"S": stock},
                        ':g': {"S": qty},
                        ':h': {"S": price},
                        ':i': {"S": mrp},
                        ':j': {"S": membership_price},
                        ':y': {"S": " "},
                        ':z': {"S": catfilename},
                        ':u': {"S": datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%fZ")}
                    },
                    ExpressionAttributeNames={
                        "#uni": "unit"
                    },
                    ReturnValues="UPDATED_NEW",
                )
                return redirect(url_for("productview"))
        else:
            return redirect(url_for("productedit"))


@app.route('/addproduct', methods=['GET', 'POST'])
def addproduct():
    if request.method == "GET":
        client = boto3.client('dynamodb', region_name=region_name, aws_access_key_id=aws_access_key_id,
                              aws_secret_access_key=aws_secret_access_key)

        response = client.scan(
            TableName='TryonceSubCategories-tloujol3mvdsveoacsa5zlf5fa-tryprod'
        )
        response3 = client.scan(
            TableName='TryonceCategories-tloujol3mvdsveoacsa5zlf5fa-tryprod'
        )
        return render_template('addproduct.html', active2='active', data=response["Items"], data3=response3["Items"])
    if request.method == "POST":
        try:
            suboday = request.form['suboday']
        except:
            suboday = "off"

        product_name = request.form['product_name']
        # product_image = request.form['product_image'] #catimage is product_image
        product_id = request.form['product_id']
        id = request.form['product_id']
        description = request.form['description']
        tryonceProductsSubcategoryId = request.form['subcat_id']
        subcat_id = request.form['subcat_id']
        unit = request.form['unit']
        subscription_price = request.form['subscription_price']
        stock = request.form['stock']
        qty = request.form['qty']
        price = request.form['price']
        mrp = request.form['mrp']
        membership_price = request.form['subscription_price']

        catimage = request.files['product_image']
        catfilename = request.files['product_image'].filename
        catfilename = product_id + catfilename

        # replace below line with s3 bucket upload

        status = upload_file(catimage, catfilename, bucket)
        # status will be true if success
        if status:
            client = boto3.client('dynamodb', region_name=region_name, aws_access_key_id=aws_access_key_id,
                                  aws_secret_access_key=aws_secret_access_key)
            if suboday == "on":
                count = request.form["count"]
                # above is for subs of the day count

                randid = ''.join(random.choices(string.digits, k=9))

                response = client.put_item(
                    TableName='TryonceSubscriptionsoftheday-tloujol3mvdsveoacsa5zlf5fa-tryprod',
                    Item={
                        'id': {"S": randid},
                        'product_id': {"S": product_id},
                        'cityadmin_id': {"S": str(cityadmin_id)},
                        'count': {"N": count},
                        'createdAt': {"S": datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%fZ")},
                        'updatedAt': {"S": datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%fZ")}
                    })

            response = client.put_item(
                TableName='TryonceProducts-tloujol3mvdsveoacsa5zlf5fa-tryprod',
                Item={
                    'id': {"S": id},
                    'product_id': {"S": product_id},
                    'product_name': {"S": product_name},
                    'product_image': {"S": catfilename},
                    '_version': {"S": str(1)},
                    'description': {"S": description},
                    'tryonceProductsSubcategoryId': {"S": tryonceProductsSubcategoryId},
                    'subcat_id': {"S": subcat_id},
                    'unit': {"S": unit},
                    'subscription_price': {"S": subscription_price},
                    'stock': {"S": stock},
                    'qty': {"S": qty},
                    'price': {"S": price},
                    'mrp': {"S": mrp},
                    'membership_price': {"S": membership_price},
                    'tryonceProductsSubscriptionofthedayId': {"S": randid},
                    'createdAt': {"S": datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%fZ")},
                    'updatedAt': {"S": datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%fZ")}
                })

            return redirect(url_for("productview"))
        else:
            return redirect(url_for("addproduct"))


# new code end ************* here ***********************************************************************************


def sendemail():
    port = int(config['EMAIL']['SSLPORT'])
    smtp_server = config['EMAIL']['SERVER']
    sender_email = config['EMAIL']['SENDER']
    password = config['EMAIL']['PASSWORD']
    username = config['EMAIL']['USERNAME']
    sendmail = config['EMAIL']['SENDMAIL']
    if sendmail != "ON":
        print("Email send disabled")
        return
    message = """\
    Subject: Important Alerts

        {msgbody}""".format(msgbody=config['EMAIL']['MESSAGE'])

    context = ssl.create_default_context()
    with smtplib.SMTP_SSL(smtp_server, port, context=context) as server:
        server.login(username, password)
        receiveremails = config['EMAIL']['RECIPIENTS'].split(',')
        for email in receiveremails:
            if email is not None and email != "":
                server.sendmail(sender_email, email, message)
                print("Email sent to: " + email)


@app.route('/')
def start():

    if current_user.is_authenticated:

        return redirect(url_for('homeview'))
    else:
        return redirect(url_for('login'))


users = {admin_username : {'password': admin_password}}


class User(flask_login.UserMixin):
    pass


@login_manager.user_loader
def user_loader(email):
    if email not in users:
        return
    user = User()
    user.fullname = "Admin"
    user.id = email
    user.email = email
    user.mobile = "xxxxxxxxxx"
    user.isadmin = 'true'
    return user



@app.route('/login', methods=['GET', 'POST'])
def login():

    if not current_user.is_authenticated:
        if request.method == 'POST':
            email = request.form['email']
            password = request.form['password']

            if admin_username == email and password == admin_password:
                print("hy")
                user = User()
                user.id = email
                user.fullname = "Admin"
                user.email = email
                user.mobile = '0000000000'
                user.isadmin = 'true'
                login_user(user)
                return redirect(url_for('homeview'))
            else:
                return render_template('login/login.html', msg='Incorrect Email Or Password')
        else:
            # return render_template('login/login.html', msg='Incorrect Email Or Password')
            return render_template('login/login.html')
    return redirect(url_for('start'))


@app.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('login'))



@app.route('/profile')
@login_required
def profile():
    user = current_user

    return render_template('profile.html', user=user)



@login_manager.unauthorized_handler
def userunauth():
    flash('Unauthorized User', 'info')
    return redirect(url_for('start'))


@app.errorhandler(404)
def not_found(e):
    return render_template("errors/404.html")


if __name__ == "__main__":
    app.run(debug=False, port=5031)
    # serve(app, host='0.0.0.0', port=5019)
